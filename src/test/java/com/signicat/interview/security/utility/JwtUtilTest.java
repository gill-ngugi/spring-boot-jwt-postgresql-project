package com.signicat.interview.security.utility;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
class JwtUtilTest {
    @Value("${jwt.secret}")
    String key;

    @Test
    void setup() {
        assertNotNull(key, "Please set some key first");
    }
}