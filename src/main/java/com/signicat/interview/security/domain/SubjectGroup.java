package com.signicat.interview.security.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Entity()
@Table(name = "user_group", uniqueConstraints = {@UniqueConstraint(columnNames = {"group_id", "subject_id"})})
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SubjectGroup implements Serializable {
    @ManyToOne()
    @JoinColumn(name = "group_id", table = "user_group")
    Group group;
    @ManyToOne()
    @JoinColumn(name = "subject_id", table = "user_group")
    Subject subject;
    boolean isBlocked = false;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
}
