package com.signicat.interview.security.data;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class DeleteSubjectFromGroup {
    @NotNull
    Integer groupId;
    Integer subjectId;
}
