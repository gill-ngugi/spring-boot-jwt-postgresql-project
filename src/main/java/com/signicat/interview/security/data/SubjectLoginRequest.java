package com.signicat.interview.security.data;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@Builder

public class SubjectLoginRequest {
    @NotBlank
    String username;
    @NotBlank
    String password;
}
