package com.signicat.interview.security.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserGroupRequest {
    @NotBlank
    String groupName;
    int groupId;
}
