package com.signicat.interview.security.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class UpdateUserGroupRequest {
    String groupName;
    @NotNull

    int groupId;
}