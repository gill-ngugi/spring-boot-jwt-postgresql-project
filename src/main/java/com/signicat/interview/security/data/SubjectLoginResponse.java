package com.signicat.interview.security.data;

import com.signicat.interview.security.domain.Group;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class SubjectLoginResponse {
    String name;
    List<Group> groups;
}
