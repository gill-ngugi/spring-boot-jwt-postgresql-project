package com.signicat.interview.security.data;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class SubjectRegisterRequest {
    @NotBlank
    String username;
    @NotBlank
    String password;

    Integer groupId;

}
