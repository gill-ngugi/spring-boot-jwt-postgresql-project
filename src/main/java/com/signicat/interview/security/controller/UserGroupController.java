package com.signicat.interview.security.controller;

import com.signicat.interview.security.data.UpdateUserGroupRequest;
import com.signicat.interview.security.data.UserGroupRequest;
import com.signicat.interview.security.domain.Group;
import com.signicat.interview.security.service.GroupServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class UserGroupController {

    GroupServiceImpl groupService;

    UserGroupController(@Autowired GroupServiceImpl groupService) {
        this.groupService = groupService;
    }

    @PostMapping("group")
    public ResponseEntity<Object> createGroup(@Validated @RequestBody UserGroupRequest userGroupRequest) {
        Group group = groupService.createUserGroup(userGroupRequest);
        return ResponseEntity.ok(group);
    }

    @GetMapping("group/{id}")
    public ResponseEntity<Object> getGroup(@PathVariable String id) {

        Optional<Group> group = groupService.getUserGroupById(Integer.parseInt(id));

        return ResponseEntity.ok(group.get());

    }

    @DeleteMapping("group/{id}")
    public ResponseEntity<Object> deleteGroup(@PathVariable Integer id) {
        groupService.deleteUserGroup(id);
        return ResponseEntity.ok(groupService.getAllUserGroup());
    }

    @PutMapping("group")
    public ResponseEntity<Object> updateGroup(@Validated @RequestBody UpdateUserGroupRequest userGroupRequest) {
        //  return ResponseEntity.ok( userGroupRequest);
        return ResponseEntity.ok(groupService.updateUserGroup(userGroupRequest));
    }

    @GetMapping("group")
    public ResponseEntity<Object> getAllGroups() {
        return ResponseEntity.ok(groupService.getAllUserGroup());
    }


}
