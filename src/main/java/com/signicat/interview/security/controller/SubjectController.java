package com.signicat.interview.security.controller;

import com.signicat.interview.security.data.AddSubjectToGroupRequest;
import com.signicat.interview.security.data.DeleteSubjectFromGroup;
import com.signicat.interview.security.data.SubjectLoginRequest;
import com.signicat.interview.security.data.SubjectRegisterRequest;
import com.signicat.interview.security.domain.Subject;
import com.signicat.interview.security.service.GroupManagementServiceImpl;
import com.signicat.interview.security.service.SubjectServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.nio.file.attribute.UserPrincipalNotFoundException;
import java.util.Optional;

@RestController
public class SubjectController {

    SubjectServiceImpl subjectService;
    GroupManagementServiceImpl groupManagementService;

    SubjectController(@Autowired SubjectServiceImpl subjectService, @Autowired GroupManagementServiceImpl groupManagementService) {
        this.subjectService = subjectService;
        this.groupManagementService = groupManagementService;
    }

    @PostMapping("/login")
    public ResponseEntity loginUser(@Validated @RequestBody SubjectLoginRequest subjectLoginRequest) throws UserPrincipalNotFoundException {
        Optional<Subject> user;
        try {
            user = this.subjectService.loginUser(subjectLoginRequest);
            return ResponseEntity.ok(user);
        } catch (UserPrincipalNotFoundException userPrincipalNotFoundException) {
            throw userPrincipalNotFoundException;
        }


    }

    @PostMapping("/register")
    public Optional<Subject> register(@Validated @RequestBody SubjectRegisterRequest subjectRegisterRequest) {
        Subject savedSub = subjectService.registerUser(subjectRegisterRequest);
        return Optional.of(savedSub);
    }

    ;

    @PostMapping("/subject/group/add")
    public ResponseEntity addSubjectToGroup(@RequestBody AddSubjectToGroupRequest addSubjectToGroupRequest) {
        groupManagementService.addSubjectToGroup(addSubjectToGroupRequest);
        return null;
    }

    @DeleteMapping("/subject/group/remove")
    public ResponseEntity removeSubjectFromGroup(@RequestBody DeleteSubjectFromGroup deleteSubjectFromGroup) {
        groupManagementService.removeSubjectFromGroup(deleteSubjectFromGroup);
        return null;
    }
}
