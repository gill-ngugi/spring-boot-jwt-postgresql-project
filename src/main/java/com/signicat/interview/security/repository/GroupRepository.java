package com.signicat.interview.security.repository;

import com.signicat.interview.security.domain.Group;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface GroupRepository extends JpaRepository<Group, Integer> {
    public Optional<Group> findUserGroupById(Integer id);

}
