package com.signicat.interview.security.repository;

import com.signicat.interview.security.domain.SubjectGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UserGroupRepository extends JpaRepository<SubjectGroup, Integer> {
    @Modifying
    @Query(value = "DELETE  FROM user_group WHERE subject_id = ?1 AND group_id = ?2", nativeQuery = true)
    public void deleteSubjectFromGroup(Integer subjectId, Integer groupId);

}
