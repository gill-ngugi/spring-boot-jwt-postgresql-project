package com.signicat.interview.security.repository;

import com.signicat.interview.security.domain.Subject;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SubjectRepository extends JpaRepository<Subject, Integer> {
    @Query(value = "SELECT * FROM subjects WHERE user_name = ?1 limit 1", nativeQuery = true)
    public Optional<Subject> findSubjectByUserName(String name);
}
