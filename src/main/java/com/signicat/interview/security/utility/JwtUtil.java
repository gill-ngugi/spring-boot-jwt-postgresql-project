package com.signicat.interview.security.utility;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator;
import com.auth0.jwt.algorithms.Algorithm;
import com.signicat.interview.security.domain.Group;
import com.signicat.interview.security.domain.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class JwtUtil {
    Logger logger = LoggerFactory.getLogger(JwtUtil.class);
    @Value(value = "${jwt.secret}")

    private String SECRET_KEY;
    @Value(value = "${jwt.expiry}")
    private String EXPIRATION_TIME; // 15 mins

    public String extractUserId(String token) {
        String user = JWT.require(Algorithm.HMAC512(SECRET_KEY.getBytes()))
                .build()
                .verify(token)
                .getSubject();
        return user;

    }

    private Boolean isTokenExpired(String token) {
        return JWT.require(Algorithm.HMAC512(SECRET_KEY.getBytes()))
                .build().verify(token).getExpiresAt().before(new Date());

    }

    @Deprecated
    public String generateToken(UserDetails userDetails) {
        Map<String, List> claims = new HashMap<>();
        return createToken(new ArrayList<>(), userDetails.getUsername(), userDetails.getUsername());
    }

    public String createToken(List<Group> claims, String subject, String username) {
        JWTCreator.Builder builder = JWT.create()
                .withSubject(subject);

        List<HashMap> groups = new ArrayList<>();

        for (Group g : claims
        ) {

            HashMap group = new HashMap<>();
            group.put("name", g.getName());
            group.put("id", g.getId());
            groups.add(group);
        }
        logger.info("user " + username + "belongs to  " + groups.size() + " groups namely " + groups);
        // List value= List.of(groups);
        builder.withClaim("username", username);
        builder.withClaim("groups", groups);
        logger.info("Jwt will expire in  " + EXPIRATION_TIME + "ms");
        String token =
                builder
                        .withExpiresAt(new Date(System.currentTimeMillis() + Long.parseLong(EXPIRATION_TIME)))
                        .sign(Algorithm.HMAC512(SECRET_KEY.getBytes()));
        return token;
    }

    public Boolean validateToken(String token, UserDetails userDetails) {
        final String id = extractUserId(token);
        return ((id.equals(((Subject) userDetails).getId().toString())) && !isTokenExpired(token));
    }

}