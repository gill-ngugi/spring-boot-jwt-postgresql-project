package com.signicat.interview.security.service;

import com.signicat.interview.security.data.UpdateUserGroupRequest;
import com.signicat.interview.security.data.UserGroupRequest;
import com.signicat.interview.security.domain.Group;

import java.util.List;
import java.util.Optional;

public interface GroupService {
    public Group createUserGroup(UserGroupRequest userGroupRequest);

    public void deleteUserGroup(Integer id);

    public Group updateUserGroup(UpdateUserGroupRequest userGroupRequest);

    public Optional<Group> getUserGroupById(Integer id);

    public List<Group> getAllUserGroup();
}
