package com.signicat.interview.security.service;

import com.signicat.interview.security.data.SubjectLoginRequest;
import com.signicat.interview.security.data.SubjectRegisterRequest;
import com.signicat.interview.security.domain.Subject;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.nio.file.attribute.UserPrincipalNotFoundException;
import java.util.Optional;


public interface SubjectService extends UserDetailsService {

    public Optional<Subject> findSubjectByName(String name);

    public Optional<Subject> loginUser(SubjectLoginRequest subjectLoginRequest) throws UserPrincipalNotFoundException;

    Subject registerUser(SubjectRegisterRequest subject);

    UserDetails findUserById(String userId);

}
