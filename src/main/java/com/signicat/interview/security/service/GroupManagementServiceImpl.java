package com.signicat.interview.security.service;

import com.signicat.interview.security.data.AddSubjectToGroupRequest;
import com.signicat.interview.security.data.DeleteSubjectFromGroup;
import com.signicat.interview.security.domain.Group;
import com.signicat.interview.security.domain.Subject;
import com.signicat.interview.security.domain.SubjectGroup;
import com.signicat.interview.security.repository.UserGroupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class GroupManagementServiceImpl implements GroupManagementService {
    UserGroupRepository subjectRepository;


    GroupManagementServiceImpl(@Autowired UserGroupRepository userGroupRepository) {
        this.subjectRepository = userGroupRepository;
    }

    @Override
    public void removeSubjectFromGroup(DeleteSubjectFromGroup deleteSubjectFromGroup) {
        Integer subjectId = deleteSubjectFromGroup.getSubjectId();
        if (subjectId == null) {
            subjectId = ((Subject) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId();
        }
        subjectRepository.deleteSubjectFromGroup(subjectId, deleteSubjectFromGroup.getGroupId());

    }

    @Override
    public void addSubjectToGroup(AddSubjectToGroupRequest addSubjectToGroupRequest) {
        Integer subjectId = addSubjectToGroupRequest.getSubjectId();
        if (subjectId == null) {
            subjectId = ((Subject) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getId();
        }
        SubjectGroup subjectGroup = SubjectGroup.builder()
                .group(Group.builder().id(addSubjectToGroupRequest.getGroupId()).build())
                .subject((Subject.builder().id(subjectId).build())).build();
        subjectRepository.save(subjectGroup);
    }
}
