package com.signicat.interview.security.service;

import com.signicat.interview.security.data.UpdateUserGroupRequest;
import com.signicat.interview.security.data.UserGroupRequest;
import com.signicat.interview.security.domain.Group;
import com.signicat.interview.security.repository.GroupRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@Service
public class GroupServiceImpl implements GroupService {
    Logger logger = LoggerFactory.getLogger(GroupServiceImpl.class);
    GroupRepository groupRepository;

    GroupServiceImpl(@Autowired GroupRepository groupRepository) {
        this.groupRepository = groupRepository;
    }

    @Override
    public Group createUserGroup(UserGroupRequest userGroupRequest) {

        return groupRepository.save(Group.builder().name(userGroupRequest.getGroupName()).build());

    }

    @Override
    public void deleteUserGroup(Integer id) {
        groupRepository.deleteById(id);

    }

    @Override
    public Group updateUserGroup(UpdateUserGroupRequest userGroupRequest) {
        logger.info("update group" + userGroupRequest.getGroupId());
        Optional<Group> group = groupRepository.findUserGroupById(userGroupRequest.getGroupId());
        if (group.isEmpty()) {
            throw new EntityNotFoundException("Group not found");
        }

        return groupRepository.save(Group.builder().name(userGroupRequest.getGroupName()).id(userGroupRequest.getGroupId()).build());

    }

    @Override
    public Optional<Group> getUserGroupById(Integer id) {
        Optional<Group> group = groupRepository.findUserGroupById(id);
        if (group.isEmpty()) {
            throw new EntityNotFoundException("Group not found");
        }
        return group;

    }

    @Override
    public List<Group> getAllUserGroup() {
        return groupRepository.findAll();

    }
}
