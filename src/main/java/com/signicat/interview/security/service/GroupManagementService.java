package com.signicat.interview.security.service;

import com.signicat.interview.security.data.AddSubjectToGroupRequest;
import com.signicat.interview.security.data.DeleteSubjectFromGroup;
import org.springframework.transaction.annotation.Transactional;

public interface GroupManagementService {
    @Transactional
    void removeSubjectFromGroup(DeleteSubjectFromGroup deleteSubjectFromGroup);

    void addSubjectToGroup(AddSubjectToGroupRequest addSubjectToGroupRequest);
}
