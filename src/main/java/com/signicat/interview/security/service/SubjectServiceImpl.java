package com.signicat.interview.security.service;

import com.signicat.interview.security.data.SubjectLoginRequest;
import com.signicat.interview.security.data.SubjectRegisterRequest;
import com.signicat.interview.security.domain.Group;
import com.signicat.interview.security.domain.Subject;
import com.signicat.interview.security.repository.GroupRepository;
import com.signicat.interview.security.repository.SubjectRepository;
import com.signicat.interview.security.utility.JwtUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.nio.file.attribute.UserPrincipalNotFoundException;
import java.util.*;

@Service
public class SubjectServiceImpl implements SubjectService {
    Logger logger = LoggerFactory.getLogger(SubjectServiceImpl.class);
    SubjectRepository subjectRepository;
    GroupRepository groupRepository;
    JwtUtil jwtUtil;

    private BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    SubjectServiceImpl(@Autowired SubjectRepository subjectRepository, @Autowired GroupRepository groupRepository, @Autowired JwtUtil jwtUtil) {
        this.subjectRepository = subjectRepository;
        this.groupRepository = groupRepository;
        this.jwtUtil = jwtUtil;

    }

    @Override
    public Optional<Subject> findSubjectByName(String name) {

        Optional<Subject> subject = subjectRepository.findSubjectByUserName(name);

        return subject;
    }

    public Optional<Subject> loginUser(@RequestBody SubjectLoginRequest subjectLoginRequest) throws UserPrincipalNotFoundException {
        Optional<Subject> subject = this.findSubjectByName(subjectLoginRequest.getUsername());
        if (subject.isEmpty()) {
            throw new UsernameNotFoundException("Username or password is wrong");
        } else {
            Subject sub = subject.get();
            sub.getPassword();
            if (!passwordEncoder.matches(subjectLoginRequest.getPassword(), sub.getPassword())) {
                throw new UsernameNotFoundException("Username or password is wrong");
            }

            List groups = new ArrayList(subject.get().getUserGroups());

            String jwt = jwtUtil.createToken(groups, String.valueOf(subject.get().getId()), subject.get().getUsername());

            subject.get().setJwt(jwt);
            return subject;
        }

    }

    @Override
    public Subject registerUser(SubjectRegisterRequest subject) {
        Optional<Group> group = Optional.empty();
        Subject.SubjectBuilder builder = Subject.builder();

        if (!Objects.isNull(subject.getGroupId())) {
            group = this.groupRepository.findById(subject.getGroupId());
            if (group.isPresent()) {
                builder.userGroups(Set.of(group.get()));

            }
        }

        Subject sub = subjectRepository.save(builder.userName(subject.getUsername()).password(passwordEncoder.encode(subject.getPassword())).build());


        if (sub.getUserGroups() == null) {
            sub.setUserGroups(Set.of());
        }
        String jwt = jwtUtil.createToken(new ArrayList(sub.getUserGroups()), String.valueOf(sub.getId()), sub.getUsername());
        sub.setJwt(jwt);
        return sub;

    }

    @Override
    public UserDetails findUserById(String userId) {
        Optional<Subject> subject = subjectRepository.findById(Integer.valueOf(userId));
        if (subject.isEmpty()) {
            throw new UsernameNotFoundException("User with the id not found");
        } else {
            UserDetails userDetails = subject.get();
            return userDetails;
        }

    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<Subject> subject = subjectRepository.findSubjectByUserName(username);
        if (subject.isEmpty()) {
            throw new UsernameNotFoundException("User with the name not found");
        } else {
            UserDetails userDetails = subject.get();
            return userDetails;
        }

    }


}
