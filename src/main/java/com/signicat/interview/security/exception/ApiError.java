package com.signicat.interview.security.exception;

import lombok.Builder;
import lombok.Data;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;
import java.util.List;

@Builder
@Data
public class ApiError {
    private HttpStatus httpStatus;
    private LocalDateTime localDateTime;
    private String message;
    private String debugMessage;
    private List<ApiSubError> apiSubError;

    ApiError() {
        this.localDateTime = LocalDateTime.now();
    }

    ApiError(HttpStatus status, Throwable ex) {
        this();
        this.httpStatus = status;
        this.message = "Unexpected error";
        this.debugMessage = ex.getLocalizedMessage();
    }

    ApiError(HttpStatus status, String message, Throwable ex) {
        this();
        this.httpStatus = status;
        this.message = message;
        this.debugMessage = ex.getLocalizedMessage();
    }

    public ApiError(HttpStatus httpStatus, LocalDateTime localDateTime, String message, String debugMessage, List<ApiSubError> apiSubError) {
        this.httpStatus = httpStatus;
        this.localDateTime = localDateTime;
        this.message = message;
        this.debugMessage = debugMessage;
        this.apiSubError = apiSubError;
    }
}
